package com.fhs.trans.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fhs.common.utils.ConverterUtils;
import com.fhs.common.utils.JsonUtils;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.VO;
import com.fhs.trans.listener.TransMessageListener;
import com.fhs.trans.vo.BasicVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Component;
import org.noear.solon.net.http.HttpUtils;
import org.slf4j.LoggerFactory;
import java.io.Serializable;
import java.util.*;

/**
 * 远程翻译服务
 */
@Data
@Slf4j
@Component
public class RpcTransService extends SimpleTransService {

    public static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RpcTransService.class);

    private boolean isEnableCloud = true;

    @Override
    public List<? extends VO> findByIds(List ids, Trans tempTrans, Set<String> targetFields) {
        //如果没开启springcloud 则走SimpleTransService逻辑
        if(!isEnableCloud){
            try {
                Class clazz = Class.forName(tempTrans.targetClassName());
                return findByIds(()->{
                    return transDiver.findByIds(ids, clazz,tempTrans.uniqueField(),targetFields);
                },tempTrans.dataSource());

            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("类找不到：" + tempTrans.targetClassName() );
            }
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("ids", ids);
        paramMap.put("uniqueField", tempTrans.uniqueField());
        //执行远程调用
        try {
            String respJson = HttpUtils.http(tempTrans.serviceName(), "/easyTrans/proxy/" + tempTrans.targetClassName() + "/findByIds")
                    .bodyJson(JsonUtils.map2json(paramMap)).post();
            return JSONArray.parseArray(respJson, BasicVO.class);
        } catch (Exception e) {
            log.error("trans service执行RPC Trans 远程调用错误:" + tempTrans.serviceName(), e);
        }
        return new ArrayList<>();
    }

    @Override
    public VO findById(Object id, Trans tempTrans) {
        if(!isEnableCloud){
            try {
                Class clazz = Class.forName(tempTrans.targetClassName());
                return findById(()->{
                    return transDiver.findById((Serializable) id, clazz, tempTrans.uniqueField(), new HashSet<>(Arrays.asList(tempTrans.fields())));
                },tempTrans.dataSource());
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("类找不到：" + tempTrans.targetClassName() );
            }
        }
        //执行远程调用
        try {
            String respJson = HttpUtils.http(tempTrans.serviceName(), "/easyTrans/proxy/" + tempTrans.targetClassName() + "/findById/"
                            + id + (!"".equals(tempTrans.uniqueField()) && tempTrans.uniqueField()!=null ? "?uniqueField=" + tempTrans.uniqueField() : "")).get();
            return JSONObject.parseObject(respJson,BasicVO.class);
        } catch (Exception e) {
            log.error("trans service执行RPC Trans 远程调用错误:" + tempTrans.serviceName(), e);
        }
        return null;
    }

    /**
     * 创建一个临时缓存map
     *
     * @param po    po
     * @param trans 配置
     * @return
     */
    protected Map<String, Object> createTempTransCacheMap(VO po, Trans trans,Set<String> targetFields) {
        if (!isEnableCloud) {
            return super.createTempTransCacheMap(po, trans,targetFields);
        }
        String fielVal = null;
        Map<String, Object> tempCacheTransMap = new LinkedHashMap<>();
        if (po == null) {
            return tempCacheTransMap;
        }
        BasicVO basicVO = (BasicVO) po;
        List<String> tempFields =  targetFields!=null ? new ArrayList<>(targetFields) : Arrays.asList(trans.fields());
        for (String field : tempFields) {
            fielVal = ConverterUtils.toString(basicVO.getObjContentMap().get(field));
            tempCacheTransMap.put(field, fielVal);
        }
        if (transCacheSettMap.containsKey(trans.targetClassName())) {
            TransCacheSett cacheSett = transCacheSettMap.get(trans.targetClassName());
            put2GlobalCache(basicVO.getObjContentMap(), cacheSett.isAccess(), cacheSett.getCacheSeconds(), cacheSett.getMaxCache(), po.getPkey(),
                    trans.targetClassName(), TransType.RPC);
        }
        return tempCacheTransMap;
    }

    /**
     * 配置缓存
     * @param type
     * @param cacheSett
     */
    public void setTransCache(Object type,TransCacheSett cacheSett){
        this.transCacheSettMap.put(ConverterUtils.toString(type),cacheSett);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        TransService.registerTransType(TransType.RPC, this);
        //注册刷新缓存服务
        TransMessageListener.regTransRefresher(TransType.RPC, this::onMessage);
    }

    public RpcTransService(){
        try {
            this.afterPropertiesSet();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
