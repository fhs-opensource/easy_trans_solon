package com.fhs.trans.controller;

import com.fhs.common.utils.ConverterUtils;
import com.fhs.core.trans.util.ReflectUtils;
import com.fhs.core.trans.vo.VO;
import com.fhs.trans.service.impl.SimpleTransService;
import com.fhs.trans.vo.BasicVO;
import com.fhs.trans.vo.FindByIdsQueryPayload;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.*;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 翻译服务请求转发代理
 */
@Data
@Slf4j
@Controller
@Mapping("/easyTrans/proxy")
public class TransProxyController {


    @Inject
    private SimpleTransService.SimpleTransDiver simpleTransDiver;

    /**
     * findByIds
     *
     * @param targetClass 目标类
     */
    @Post
    @Mapping("/{targetClass}/findByIds")
    public List findByIds(@PathVar String targetClass, @Body FindByIdsQueryPayload payload) throws ClassNotFoundException {

        List<? extends Serializable> ids = payload.getIds();
        Class fieldType = getPkeyFieldType(targetClass);
        // 如果字段类型不是String，则转换
        if (fieldType == int.class || fieldType == Integer.class) {
            ids = payload.getIds().stream().filter(id -> {
                return id != null && !id.isEmpty();
            }).map(Integer::valueOf).collect(Collectors.toList());
        } else if (fieldType == long.class || fieldType == Long.class) {
            ids = payload.getIds().stream().filter(id -> {
                return id != null && !id.isEmpty();
            }).map(Long::valueOf).collect(Collectors.toList());
        }
        return simpleTransDiver.findByIds(ids, (Class<? extends VO>) Class.forName(targetClass),payload.getUniqueField()).stream().map(vo -> {
            try {
                return vo2BasicVO(vo);
            } catch (IllegalAccessException e) {
                return null;
            }
        }).collect(Collectors.toList());
    }

    /**
     * 获取主键字段类型
     * @param targetClass po类名
     * @return 主键字段类型
     * @throws ClassNotFoundException 如果类不存在
     */
    private Class getPkeyFieldType(String targetClass) throws ClassNotFoundException {
        return ReflectUtils.getIdField(Class.forName(targetClass),true).getType();
    }

    /**
     * vo转basicvo
     *
     * @param vo
     * @return
     * @throws IllegalAccessException
     */
    private BasicVO vo2BasicVO(VO vo) throws IllegalAccessException {
        BasicVO result = new BasicVO();
        result.setId(ConverterUtils.toString(vo.getPkey()));
        List<Field> fields = ReflectUtils.getAllField(vo.getClass());
        for (Field field : fields) {
            field.setAccessible(true);
            result.getObjContentMap().put(field.getName(), field.get(vo));
        }
        return result;
    }

    /**
     * 根据id查询
     *
     * @param targetClass 目标类
     */
    @Mapping("/{targetClass}/findById/{id}")
    public Object findById(@PathVar String targetClass, @PathVar String id, @Param("uniqueField")String uniqueField) throws ClassNotFoundException, IllegalAccessException {
        Serializable sid = id;
        Class fieldType = getPkeyFieldType(targetClass);
        // 如果字段类型不是String，则转换
        if (fieldType == int.class || fieldType == Integer.class) {
            sid = Integer.valueOf(id);
        } else if (fieldType == long.class || fieldType == Long.class) {
            sid = Long.valueOf(id);
        }
        VO vo = simpleTransDiver.findById(sid, (Class<? extends VO>) Class.forName(targetClass),uniqueField);
        if (vo == null) {
            return null;
        }
        return vo2BasicVO(vo);
    }

}
