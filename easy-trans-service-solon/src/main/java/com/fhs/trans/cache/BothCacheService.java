package com.fhs.trans.cache;

import lombok.Data;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.cache.CacheService;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * 二级缓存管理器
 */
@Data
public class BothCacheService<T> {
    /**
     * redis key前缀
     */
    private static final String TRANS_PRE = "trans:";

    @Inject
    private CacheService cacheService;




    /**
     * 用来放字典缓存的map
     */
    private Map<String, T> localCacheMap = new ConcurrentHashMap<>();

    /**
     * 添加缓存
     *
     * @param key       key
     * @param value     value
     * @param onlyLocal 是否只添加本地缓存
     */
    public void put(String key, T value, boolean onlyLocal) {
        if (!onlyLocal && cacheService != null) {
            cacheService.store(TRANS_PRE + key, value,0);
        }
        localCacheMap.put(key, value);
    }

    /**
     * 获取本地缓存
     *
     * @param key key
     * @return value
     */
    public T get(String key) {
        if (localCacheMap.containsKey(key)) {
            return localCacheMap.get(key);
        }
        if (cacheService != null) {
            T result = (T)cacheService.get(TRANS_PRE + key);
            if (result != null) {
                localCacheMap.put(key, result);
            }
            return result;
        }
        return null;
    }

    /**
     * 模糊删除key
     *
     * @param keyStartWith key
     */
    public void remove(String keyStartWith) {
        Set<String> keys = localCacheMap.keySet().stream().filter(key -> {
            return key.startsWith(keyStartWith);
        }).collect(Collectors.toSet());
        for (String key : keys) {
            localCacheMap.remove(key);
        }
    }

}
