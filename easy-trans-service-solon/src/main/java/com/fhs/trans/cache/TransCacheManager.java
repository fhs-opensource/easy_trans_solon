package com.fhs.trans.cache;

import com.fhs.common.utils.ConverterUtils;
import com.fhs.common.utils.JsonUtils;
import com.fhs.core.trans.constant.TransType;
import com.fhs.trans.service.impl.RpcTransService;
import com.fhs.trans.service.impl.SimpleTransService;
import lombok.Data;
import org.noear.solon.cloud.CloudClient;
import org.noear.solon.cloud.model.Event;

import java.util.HashMap;
import java.util.Map;

/**
 * 缓存管理器
 */
@Data
public class TransCacheManager {


    private RpcTransService rpcTransService;

    private SimpleTransService simpleTransService;

    private boolean isEnableCloud;


    /**
     * 清理缓存
     *
     * @param targetClass 目标
     * @param pkey        主键
     */
    public void clearCache(Class targetClass, Object pkey) {
        if (simpleTransService != null) {
            simpleTransService.clearGlobalCache(pkey, targetClass.getName(), TransType.SIMPLE);
        }
        if (rpcTransService != null) {
            rpcTransService.clearGlobalCache(pkey, targetClass.getName(), TransType.RPC);
        }
        //如果使用了微服务则发送事件
        if (isEnableCloud) {
            Map<String, String> body = new HashMap<>();
            body.put("messageType", "clear");
            body.put("target", targetClass.getName());
            body.put("pkey", ConverterUtils.toString(pkey));
            body.put("transType", TransType.SIMPLE);
            CloudClient.event().publish(
                    new Event("trans", JsonUtils.map2json(body)));
            body.put("transType", TransType.RPC);
            CloudClient.event().publish(
                    new Event("trans", JsonUtils.map2json(body)));
        }
    }

    /**
     * 配置RPC 翻译的缓存
     *
     * @param targetClassName 目标类
     * @param cacheSett       缓存配置
     */
    public void setRpcTransCache(String targetClassName, SimpleTransService.TransCacheSett cacheSett) {
        rpcTransService.setTransCache(targetClassName, cacheSett);
    }


}
