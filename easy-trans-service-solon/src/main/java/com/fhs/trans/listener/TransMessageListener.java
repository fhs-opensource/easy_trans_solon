package com.fhs.trans.listener;

import com.fhs.common.utils.JsonUtils;
import com.fhs.trans.fi.TransRefresher;
import org.noear.solon.cloud.CloudEventHandler;
import org.noear.solon.cloud.annotation.CloudEvent;
import org.noear.solon.cloud.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 翻译服务redis消息接收转发器
 *
 * @author user
 * @date 2020-05-19 10:22:15
 */
@CloudEvent("trans")
public class TransMessageListener implements CloudEventHandler {

    /**
     * 日志记录
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TransMessageListener.class);
    /**
     * key trans Type value是transtype对应 的 缓存刷新方法
     */
    private static Map<String, TransRefresher> transRefresherMap = new HashMap<>();


    /**
     * 注册消息刷新器
     *
     * @param transType      transtype
     * @param transRefresher 对应的消息刷新器(functioninterface)
     */
    public static void regTransRefresher(String transType, TransRefresher transRefresher) {
        transRefresherMap.put(transType, transRefresher);
    }

    @Override
    public boolean handle(Event event) throws Throwable {
        LOGGER.info("trans cache listener  received  a msg:" + event.content());
        Map<String, Object> messageMap = JsonUtils.parseJSON2Map( event.content());
        if (transRefresherMap.containsKey(messageMap.get("transType"))) {
            transRefresherMap.get(messageMap.get("transType")).refreshCache(messageMap);
        } else {
            LOGGER.error(messageMap.get("transType") + "没有实现对应的缓存刷新器");
        }
        return true;
    }
}
