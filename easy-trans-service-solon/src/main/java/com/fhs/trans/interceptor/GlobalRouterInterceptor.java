package com.fhs.trans.interceptor;

import com.fhs.trans.service.impl.TransService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Handler;
import org.noear.solon.core.route.RouterInterceptor;
import org.noear.solon.core.route.RouterInterceptorChain;

/**
 * 全局路由拦截器
 *
 * @author noear
 */
@Component
public class GlobalRouterInterceptor implements RouterInterceptor {
    @Inject
    private TransService transService;

    @Override
    public void doIntercept(Context ctx, Handler mainHandler, RouterInterceptorChain chain) throws Throwable {
        chain.doIntercept(ctx, mainHandler);
    }

    @Override
    public Object postResult(Context ctx, Object result) throws Throwable {
        //此处为拦截处理
        if (result != null && !(result instanceof Throwable) && ctx.action() != null) {
            result = transService.transOneLoop(result, true);
        }

        return result;
    }
}
