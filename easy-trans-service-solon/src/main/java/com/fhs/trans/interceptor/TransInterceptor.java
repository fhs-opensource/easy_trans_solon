package com.fhs.trans.interceptor;

import com.fhs.trans.service.impl.TransService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;

/**
 * 翻译拦截器
 *
 */
@Component
public class TransInterceptor implements Interceptor {

    @Inject
    private TransService transService;

    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        //此处为拦截处理
        Object rst = inv.invoke();
        if (rst != null) {
            rst = transService.transOneLoop(rst, true);
        }

        return rst;
    }
}
