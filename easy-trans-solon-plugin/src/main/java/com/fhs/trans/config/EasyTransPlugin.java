package com.fhs.trans.config;

import com.fhs.core.trans.anno.TransMethodResult;
import com.fhs.trans.interceptor.GlobalRouterInterceptor;
import com.fhs.trans.interceptor.TransInterceptor;
import com.fhs.trans.listener.TransMessageListener;
import com.fhs.trans.service.impl.RpcTransService;
import org.noear.solon.core.AppContext;
import org.noear.solon.core.Plugin;

public class EasyTransPlugin implements Plugin {


    @Override
    public void start(AppContext context) {
        //增加云启用控制
        if (context.cfg().getBool("easy-trans.enable-cloud", false)) {
            context.beanMake(TransMessageListener.class);
            context.beanMake(RpcTransService.class);
            context.beanScan("com.fhs.trans.controller");
        }
        //增加云启用控制
        if (context.cfg().getBool("easy-trans.enable-global", false)) {
            context.beanMake(GlobalRouterInterceptor.class);
        }

        TransInterceptor transInterceptor = context.beanMake(TransInterceptor.class).get();
        context.beanInterceptorAdd(TransMethodResult.class, transInterceptor);
        context.beanScan("com.fhs.trans.config");
    }
}
