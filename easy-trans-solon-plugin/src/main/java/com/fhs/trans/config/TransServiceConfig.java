package com.fhs.trans.config;

import com.fhs.trans.cache.BothCacheService;
import com.fhs.trans.cache.TransCacheManager;
import com.fhs.trans.extend.MybatisPlusSimpleTransDiver;
import com.fhs.trans.service.impl.*;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

@Configuration
public class TransServiceConfig {

    @Inject("${easy-trans.enable-cloud:false}")
    private boolean isEnableCloud;

    @Bean()
    public DictionaryTransService dictionaryTransService(@Inject BothCacheService bothCacheService) throws Exception {
        DictionaryTransService result = new DictionaryTransService();
        result.setBothCacheService(bothCacheService);
        result.afterPropertiesSet();
        return result;
    }

    @Bean()
    public TransCacheManager transCacheManager(@Inject SimpleTransService simpleTransService,@Inject(required = false) RpcTransService rpcTransService){
        TransCacheManager result = new  TransCacheManager();
        result.setRpcTransService(rpcTransService);
        result.setSimpleTransService(simpleTransService);
        result.setEnableCloud(isEnableCloud);
        return result;
    }


    @Bean()
    public BothCacheService bothCacheService() {
        BothCacheService result = new BothCacheService();
        return result;
    }


    @Bean()
    public EnumTransService enumTransService() {
        EnumTransService result = new EnumTransService();
        result.afterPropertiesSet();
        return result;
    }

    @Bean()
    public MybatisPlusSimpleTransDiver MybatisPlusSimpleTransDiver() {
        return new MybatisPlusSimpleTransDiver();
    }

    @Bean()
    public SimpleTransService simpleTransService(@Inject MybatisPlusSimpleTransDiver transDiver) throws Exception {
        SimpleTransService result = new SimpleTransService();
        result.afterPropertiesSet();
        result.regsiterTransDiver(transDiver);
        return result;
    }

    @Bean()
    public TransService transService() {
        TransService result = new TransService();
        return result;
    }

}
