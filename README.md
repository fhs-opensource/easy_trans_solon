# easy_trans

# 介绍

## easy trans适用于5种场景   

1   我有一个id，但是我需要给客户展示他的title/name  但是我又不想自己手动做表关联查询   
2   我有一个字典码 sex  和 一个字典值0  我希望能翻译成   男  给客户展示。   
3   我有一组user id 比如 1，2,3  我希望能展示成 张三,李四,王五 给客户   
4   我有一个枚举，枚举里有一个title字段，我想给前端展示title的值 给客户     
5   我有一个唯一键(比如手机号，身份证号码，但是非其他表id字段)，但是我需要给客户展示他的title/name  但是我又不想自己手动做表关联查询   

# 食用步骤
## 技术经理/架构 需要做的事情
#### 1、先把maven 引用加上
``` xml
       <dependency>
            <groupId>com.fhs-opensource</groupId>
            <artifactId>easy-trans-solon-plugin</artifactId>
            <version>1.3.1</version>
        </dependency>
```
 
注意：非maven中央仓库更新可能延迟，如果高版本无法引入请尝试切到低一个版本过一天后在切回来。   不要使用阿里云仓库，因为他们很久没和中央仓库同步了


#### 2、初始化字典数据(如果你们项目没字典表请忽略)
 ``` java
         @Inject
         private DictionaryTransService dictionaryTransService;
	    //在某处将字典缓存刷新到翻译服务中，以下是demo
	    Map<String,String> transMap = new HashMap<>();
        transMap.put("0","男");
        transMap.put("1","女");
        dictionaryTransService.refreshCache("sex",transMap);
```  
#### 3、如果是微服务模式的话，记得使用redis来存放字典缓存
 ``` yaml
       easy-trans:
            enable-cloud: true #开启cloud模式
            cache:
              driverType: "redis" #驱动类型
              server: "192.168.0.213:6379"
              password: "123456"
```  
``` java
         @Inject
         BothCacheService bothCacheService;
	    
		 //RedissonCacheService  也支持
		 @Inject(value = "${easy-trans.cache}")
         RedisCacheService cacheService; 
		 // 这行代码要放到 dictionaryTransService.refreshCache("sex",transMap);前面才管用
		 bothCacheService.setCacheService(cacheService);

```


#### 4、缓存说明

很高兴的告诉大家，Easy trans的强大缓存功能上线了，下面告诉大家怎么用。    

SIMPLE加缓存和清理缓存：

``` java
//在PO上标记 下面注解，就可以添加缓存
@TransDefaultSett(isUseCache = true,cacheSeconds = 20)
public class User{

}
@Inject
TransCacheManager transCacheManager;
//下面的清理缓存
transCacheManager.clearCache(AppxModel.class,1);
```
下面是RPC添加缓存，RPC清理缓存和SIMPLE相同，就不贴代码了
``` java
transCacheManager.setRpcTransCache("com.fhs.uc.User", SimpleTransService.TransCacheSett.builder() 
   .maxCache(100).cacheSeconds(20).isAccess(false).build());
```
要点：因为RPC清理缓存是基于solon的分布式事件总线做的，所以要想生效需要加一个总线的依赖：https://solon.noear.org/article/78    

字典缓存刷新说明：    
字典是通过BothCacheService来存放缓存数据的，为了更好的速度和适配微服务，他使用了二级缓存，如果字典有更新，需要刷新redis的缓存，并且清理掉各个微服务的进程缓存，我们提供了以下API来刷新。
``` java
 @Inject
 DictionaryTransService dictionaryTransService;
dictionaryTransService.refreshCacheAndNoticeOtherService("sex",dictSexMap);
```

## 普通程序员需要做的事情
pojo 中添加
``` java   
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
//实现TransPojo  接口，代表这个类需要被翻译或者被当作翻译的数据源
public class Student implements TransPojo {

     // 字典翻译 ref为非必填
    @Trans(type = TransType.DICTIONARY,key = "sex",ref = "sexName")
    private Integer sex;

    //这个字段可以不写，实现了TransPojo接口后有一个getTransMap方法，sexName可以让前端去transMap取
    private String sexName;
    
    //SIMPLE 翻译，用于关联其他的表进行翻译    schoolName 为 School 的一个字段
    @Trans(type = TransType.SIMPLE,target = School.class,fields = "schoolName")
    private String schoolId;
	
	 @Trans(type = TransType.RPC,targetClassName = "com.fhs.uc.User",fields = "userName",serviceName = "ucenter")
    private String createUserId;
	
    // 枚举翻译，返回文科还是理科给前端
    @Trans(type=TransType.ENUM,key = "desc")
    private StudentType studentType = StudentType.ARTS;

    public static enum StudentType{

        ARTS("文科"),
        SCIENCES("理科");

        private String desc;
        StudentType(String desc){
            this.desc = desc;
        }
    }
}

```
controller
 ``` java
    @Inject
    TransService transService;
    @Mapping("trans")
    public Student trans() {
        Student stu = new Student();
        stu.setId("11");
        stu.setAppId(1);
        stu.setSex("boy");
		stu.setCreateUserId("1")
        //翻译集合用：transMore方法  如果pojo的子属性还需要被翻译用： transOneLoop/transManyLoop 方法
        transService.transOne(stu);
        return stu;
    }
 ``` 
然后访问你的controller，看返回结果。


## easy trans solon 版本 支持的3种类型    

### 字典翻译(TransType.DICTIONARY)

 需要使用者把字典信息刷新到DictionaryTransService 中进行缓存，使用字典翻译的时候取缓存数据源 
	 
###    简单翻译(TransType.SIMPLE)
比如有userId需要userName或者userPo给前端，原理是组件使用MybatisPlus/JPA的API自动进行查询，把结果放到TransMap中。  

###    跨微服务翻译(TransType.RPC) 
原理同简单翻译，只不过可以跨不同微服务   

###    枚举翻译(TransType.ENUM) 
比如我要把SEX.BOY 翻译为男，可以用枚举翻译。   

### 平铺模式    
默认easy trans会把翻译结果放到transMap属性中，比如你取sexName(性别) 需要调用transMap.sexName。如果前端觉得不方便可以使用平铺模式transService.transOneLoop(stu,true); 需要您的json序列化方式为jackson或者fastjson.



# 参与贡献和技术支持

 如果遇到使用问题可以加QQ群:976278956   
 如果你们使用了此插件，请留下单位名称。



# 插件文档

下面文档是springboot版本的，除了包引入有差距和不支持AUTO类型的翻译之外其他的都和solon版本一致

https://gitee.com/fhs-opensource/easy_trans/wikis/%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B


如果您觉得本软件好用，请帮忙给个star吧，如果您能发动身边的同项目组同事一起给个star那就太感谢您了
